all: net-inventorylib

remake: clean all

net-inventorylib:
	./setup.py sdist bdist_wheel

upload:
	python3 -m twine upload dist/*

clean:
	rm -rf build dist net_inventorylib.egg-info
