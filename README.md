net-inventorylib
================

This package manages reading and processing network configuration inventories.

It contains functionality that was in net-genconfig but separated out so it
can be used by different utilities consistently.
